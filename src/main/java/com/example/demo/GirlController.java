package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "girl")
public class GirlController {
    
    @Autowired
    private GirlConfig girlConfig;
    
    
//    @GetMapping(value = "/girl")
    @RequestMapping(value = {"/test"},method = RequestMethod.GET)
    public String test(){
        return girlConfig.getCupSize();
    }
}
